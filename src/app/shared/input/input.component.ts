import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styles: [ 'p { color: red; }' ]
})
export class InputComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
