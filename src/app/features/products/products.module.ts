import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './components/product/product.component';
import { ProductsComponent } from './products.component';
import { InputModule } from '../../shared/input/input.module';
import { ButtonModule } from '../../shared/button/button.module';


@NgModule({
  declarations: [
    ProductsComponent,
    ProductComponent
  ],
  imports: [
    CommonModule,
    InputModule,
    ButtonModule
  ],
  exports: [ ProductsComponent ]
})
export class ProductsModule {
}
